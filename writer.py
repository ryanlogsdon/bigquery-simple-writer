from google.cloud import bigquery
import os

credentials_path = '/PATH/TO/YOUR/SERVICE/ACCOUNT/KEY/myKey.privateKey.json'
os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = credentials_path

client = bigquery.Client()
table_id = 'MY_PROJECT.MY_BIGQUERY.MY_TABLE'

rows_to_insert = [
    {u'fieldName1':'myValue1', u'fieldName2':'myValue2'},
]

errors = client.insert_rows_json(table_id, rows_to_insert)
if errors == []:
    print('New rows have been added.')
else:
    print(f'Encountered errors while inserting rows: {errors}')
